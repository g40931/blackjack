package esi.atl.g40931.blackjack.controller;

import esi.atl.g40931.blackjack.data.DBManager;
import esi.atl.g40931.blackjack.data.ResultDTO;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import java.time.LocalDate;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class HistoryController {

    @FXML
    private TextField pName;

    @FXML
    private TableView<ResultDTO> table;

    @FXML
    private TableColumn<ResultDTO, String> cName;

    @FXML
    private TableColumn<ResultDTO, LocalDate> cTime;

    @FXML
    private TableColumn<ResultDTO, Number> cRounds;

    @FXML
    private TableColumn<ResultDTO, Number> cEarnings;

    @FXML
    private TableColumn<ResultDTO, Number> cAverage;

    @FXML
    private void initialize() {
        this.table.setItems(FXCollections.observableList(DBManager.INSTANCE.getHistory()));

        this.table.getColumns().forEach(c -> c.prefWidthProperty().bind(this.table.widthProperty().divide(5).subtract(0.4D)));

        cName.setCellValueFactory(f -> new SimpleStringProperty(f.getValue().getPlayerName()));
        cTime.setCellValueFactory(f -> new SimpleObjectProperty<>(f.getValue().getTime()));
        cRounds.setCellValueFactory(f -> new SimpleIntegerProperty(f.getValue().getRoundsPlayed()));
        cEarnings.setCellValueFactory(f -> new SimpleIntegerProperty(f.getValue().getEarnings()));
        cAverage.setCellValueFactory(f -> new SimpleDoubleProperty(f.getValue().getEarnings() * 1.0D / f.getValue().getRoundsPlayed()));
    }

    @FXML
    private void search(ActionEvent actionEvent) {
        this.table.setItems(FXCollections.observableList(DBManager.INSTANCE.getHistory(pName.getText())));
    }
}
