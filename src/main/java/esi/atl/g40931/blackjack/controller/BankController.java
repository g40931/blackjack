package esi.atl.g40931.blackjack.controller;

import esi.atl.g40931.blackjack.model.game.Blackjack;
import esi.atl.g40931.blackjack.model.players.Player;
import esi.atl.g40931.blackjack.mvc.Observer;
import esi.atl.g40931.blackjack.mvc.PlayerUpdate;
import esi.atl.g40931.blackjack.mvc.UpdateType;

import java.lang.ref.WeakReference;
import java.util.Arrays;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class BankController implements Observer<Player> {

    private final WeakReference<Blackjack> gameInstance;

    public BankController(Blackjack gameInstance) {
        this.gameInstance = new WeakReference<>(gameInstance);
        gameInstance.getBank().registerObserver(this);
    }

    @Override
    public void notify(Player instance, UpdateType<Player> type) {
        PlayerUpdate uType = (PlayerUpdate) type;
        switch (uType) {
            case PICK:
                System.out.println(instance.getName() + " : " + instance.getHand().getContent() + " = "
                        + Arrays.toString(instance.getHand().getPossibleValues()) + " / " + instance.getHand().getBest());
                if (instance.getHand().getContent().size() > 2) this.play(instance);
                break;
            case TURN_START:
                instance.getHand().getContent().get(0).show();
                this.play(instance);
                break;
        }
    }

    public void remove() {
        Blackjack bj = this.gameInstance.get();
        if (bj != null) bj.getBank().removeObserver(this);
    }

    private void play(Player instance) {
        Blackjack bj = gameInstance.get();
        if (bj != null) {
            if (instance.getHand().getBest() < 17) bj.hit();
            else bj.stand();
        }
    }
}
