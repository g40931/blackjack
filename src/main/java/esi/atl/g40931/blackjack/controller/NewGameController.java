package esi.atl.g40931.blackjack.controller;

import esi.atl.g40931.blackjack.model.game.BlackJackBuilder;
import esi.atl.g40931.blackjack.model.game.Blackjack;
import esi.atl.g40931.blackjack.model.players.Player;
import esi.atl.g40931.blackjack.model.players.PlayerType;
import esi.atl.g40931.blackjack.mvc.BlackjackUpdate;
import esi.atl.g40931.blackjack.mvc.Observer;
import esi.atl.g40931.blackjack.mvc.UpdateType;
import esi.atl.g40931.blackjack.view.HandView;
import esi.atl.g40931.blackjack.view.IntField;
import esi.atl.g40931.blackjack.view.MainMenu;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class NewGameController {

    private final List<HandView> openViews = new ArrayList<>();
    private final ObservableList<Pair<String, Integer>> players = FXCollections.observableArrayList();
    private Blackjack gameInstance;
    private WeakReference<Player> ourPlayer; // For when more than one player is added. Though all of this shouldn't be in NewGame !!
    private BankController bankController;
    private BlackjackObs blackjackObs;
    private boolean setOnClose = false;
    @FXML
    private TextField fieldName;

    @FXML
    private Slider sliderBet;

    @FXML
    private Button buttonStart;

    @FXML
    private Button buttonStop;

    @FXML
    private Button buttonNext;

    @FXML
    private IntField fieldMoney;

    @FXML
    private Menu menuPView;

    @FXML
    private TableView<Pair<String, Integer>> table;

    @FXML
    private TableColumn<Pair<String, Integer>, String> cName;

    @FXML
    private TableColumn<Pair<String, Integer>, Number> cMoney;

    @FXML
    private void initialize() {
        cName.prefWidthProperty().bind(table.widthProperty().divide(2D).subtract(0.9D));
        cMoney.prefWidthProperty().bind(table.widthProperty().divide(2D).subtract(0.9D));
        cName.setCellValueFactory(f -> new SimpleStringProperty(f.getValue().getKey()));
        cMoney.setCellValueFactory(f -> new SimpleIntegerProperty(f.getValue().getValue()));
        table.setItems(this.players);
    }

    @FXML
    private void about(ActionEvent event) {
        System.out.println("About");
        //TODO: implement
    }

    @FXML
    private void next(ActionEvent event) {
        if (this.gameInstance.isRoundOver()) this.gameInstance.nextRound();
        else System.err.println("This should not happen. Logging JIC");
    }

    @FXML
    private void start(ActionEvent event) {
        BlackJackBuilder builder = new BlackJackBuilder().addPlayer(PlayerType.BANK)
                .setBet((int) this.sliderBet.getValue());
        this.players.forEach(p -> builder.addPlayer(PlayerType.REGULAR, p.getKey().length() > 0 ? p.getKey() : "anonymous", p.getValue()));
        this.gameInstance = builder.build();

        this.gameInstance.registerObserver(this.blackjackObs = new BlackjackObs());

        this.buttonStart.setDisable(true);
        this.fieldMoney.setEditable(false);
        this.fieldMoney.setDisable(true);
        this.fieldName.setEditable(false);
        this.fieldName.setDisable(true);
        this.sliderBet.setDisable(true);
        this.setRoundOver(true);

        this.bankController = new BankController(this.gameInstance);

        this.initViews();

        if (!setOnClose) {
            fieldMoney.getScene().getWindow().setOnCloseRequest(e -> this.reset());
            setOnClose = true;
        }
    }

    private void initViews() {
        this.menuPView.setDisable(false);
        this.gameInstance.getPlayers().forEach(pl -> {
            MenuItem mi = new MenuItem(pl.getName());
            mi.setOnAction(evt -> {
                HandView hv = new HandView(this.gameInstance, pl);
                hv.show();
                this.openViews.add(hv);
            });
            this.menuPView.getItems().add(mi);
        });
    }

    @FXML
    private void stop(ActionEvent event) {
        this.reset();
    }

    @FXML
    private void mainMenu(ActionEvent event) {
        this.reset();
        ((Stage) this.buttonStart.getScene().getWindow()).setScene(MainMenu.mainMenu);
    }

    private void setRoundOver(boolean roundOver) {
        buttonNext.setDisable(!roundOver);
        buttonStop.setDisable(!roundOver);
    }

    // Reset & cleanup
    private void reset() {
        this.buttonStart.setDisable(false);
        this.fieldMoney.setEditable(true);
        this.fieldMoney.setDisable(false);
        this.fieldName.setEditable(true);
        this.fieldName.setDisable(false);
        this.sliderBet.setDisable(false);
        this.setRoundOver(false);

        this.menuPView.setDisable(true);
        this.menuPView.getItems().clear();

        if (this.gameInstance == null) return;
        this.gameInstance.terminate();
        this.bankController.remove();
        this.gameInstance.removeObserver(this.blackjackObs);
        this.gameInstance = null;
        this.bankController = null;
        this.blackjackObs = null;

        this.openViews.forEach(Stage::close);
        this.openViews.clear();
    }

    @FXML
    private void add(ActionEvent actionEvent) {
        String name = this.fieldName.getText();
        this.players.add(new Pair<>(name.length() > 0 ? name : "anonymous", this.fieldMoney.getValue()));
    }

    @FXML
    private void removeSelected(ActionEvent actionEvent) {
        this.players.remove(table.getSelectionModel().getSelectedItem());
    }


    /*
    The following two classes are there because java doesn't allow implementing a generic
    interface twice (with different generics) with the same class.
     */

    private class BlackjackObs implements Observer<Blackjack> {
        @Override
        public void notify(Blackjack instance, UpdateType<Blackjack> type) {
            BlackjackUpdate uType = (BlackjackUpdate) type;
            switch (uType) {
                case NEXT_ROUND_POST:
                    System.out.println("new round");
                    Platform.runLater(() -> NewGameController.this.setRoundOver(false));
                    break;
                case ROUND_OVER:
                    System.out.println("Round over. Winners: " + instance.getRoundWinners().stream().map(Player::getName).collect(Collectors.toList()));
                    Platform.runLater(() -> NewGameController.this.setRoundOver(true));
                    break;
            }
        }
    }
}
