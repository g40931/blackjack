package esi.atl.g40931.blackjack.controller;

import esi.atl.g40931.blackjack.data.DBManager;
import esi.atl.g40931.blackjack.data.ResultDTO;
import esi.atl.g40931.blackjack.model.players.Player;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.lang.ref.WeakReference;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class RecapViewController {
    private WeakReference<Player> player;
    private int rounds;

    @FXML
    private TextField pName;

    @FXML
    private TextField roundsCount;

    @FXML
    private TextField earnings;

    @FXML
    private TextField earningsAvg;

    public void init(Player player, int rounds) {
        if (player == null) return;
        this.player = new WeakReference<>(player);
        this.rounds = rounds;
        this.pName.setText(player.getName());
        this.roundsCount.setText(String.valueOf(rounds));
        int earnings = player.getMoney() - player.getInitialMoney();
        this.earnings.setText(String.valueOf(earnings));
        this.earningsAvg.setText(String.format("%.3f", earnings * 1.0D / rounds));
    }

    @FXML
    private void buttonClose(ActionEvent actionEvent) {
        ((Button) actionEvent.getSource()).getScene().getWindow().hide();
    }

    @FXML
    private void save(ActionEvent actionEvent) {
        Player player = this.player.get();
        if (player != null) DBManager.INSTANCE.save(new ResultDTO(player, this.rounds));
        ((Button) actionEvent.getSource()).setDisable(true);
    }
}
