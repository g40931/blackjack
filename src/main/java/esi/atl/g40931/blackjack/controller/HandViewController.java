package esi.atl.g40931.blackjack.controller;

import esi.atl.g40931.blackjack.model.cards.Card;
import esi.atl.g40931.blackjack.model.game.Blackjack;
import esi.atl.g40931.blackjack.model.players.Player;
import esi.atl.g40931.blackjack.mvc.*;
import esi.atl.g40931.blackjack.view.CardView;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.ImageCursor;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class HandViewController implements Observer<Player> {

    private List<CardView> cards = new ArrayList<>(7);
    private WeakReference<Blackjack> gameInstance;
    private WeakReference<Player> playerInstance;
    @FXML
    private HBox firstSet;

    @FXML
    private HBox secondSet;

    @FXML
    private CardView firstCard;

    @FXML
    private CardView secondCard;

    @FXML
    private CardView thirdCard;

    @FXML
    private CardView fourthCard;

    @FXML
    private CardView fifthCard;

    @FXML
    private CardView sixthCard;

    @FXML
    private CardView seventhCard;

    @FXML
    private Button buttonStand;

    @FXML
    private Button buttonHit;

    @FXML
    private Button buttonLeave;

    @FXML
    private TextField fieldMoney;

    private static void customCursor(Button on, String path) {
        on.setCursor(new ImageCursor(new Image(path)));
        on.getStyleClass().remove("button");
    }

    @FXML
    private void initialize() { // Little fixes due to https://bugs.openjdk.java.net/browse/JDK-8089191
        customCursor(this.buttonHit, "icons/card-draw.png");
        customCursor(this.buttonStand, "icons/poker-hand.png");
        customCursor(this.buttonLeave, "icons/card-discard.png");

        this.cards.add(firstCard);
        this.cards.add(secondCard);
        this.cards.add(thirdCard);
        this.cards.add(fourthCard);
        this.cards.add(fifthCard);
        this.cards.add(sixthCard);
        this.cards.add(seventhCard);

        this.cards.forEach(i -> {
            i.heightProperty().bind(firstSet.heightProperty().subtract(firstSet.getSpacing() * 2));
            i.widthProperty().bind(firstSet.widthProperty().divide(4).subtract(firstSet.getSpacing() * 3));
        });

        this.setSecondRowShown(false);
    }

    private void setOurTurn(boolean ourTurn) {
        buttonHit.setDisable(!ourTurn);
        buttonStand.setDisable(!ourTurn);
    }

    @FXML
    private void hit(ActionEvent event) {
        Blackjack bj = this.gameInstance.get();
        if (bj != null) bj.hit();
    }

    @FXML
    private void stand(ActionEvent event) {
        Blackjack bj = this.gameInstance.get();
        if (bj != null) bj.stand();
    }

    public void initialize(Blackjack bj, Player ourPlayer) {
        this.cleanup();
        ourPlayer.registerObserver(this);
        this.gameInstance = new WeakReference<>(bj);
        this.playerInstance = new WeakReference<>(ourPlayer);

        List<Card> pCards = ourPlayer.getHand().getContent();
        for (int i = 0; i < pCards.size(); i++) this.cards.get(i).setCard(pCards.get(i));
        if (pCards.size() > 4) this.setSecondRowShown(true);
        else this.setSecondRowShown(false);

        ourPlayer.getHand().registerObserver((i, u) -> {
            if (u.equals(HandUpdate.CLEAR)) Platform.runLater(() -> {
                cards.forEach(CardView::clear);
                this.setSecondRowShown(false);
            });
        });

        bj.registerObserver((i, u) -> {
            BlackjackUpdate bu = (BlackjackUpdate) u;
            switch (bu) {
                case NEXT_ROUND_PRE:
                    Platform.runLater(() -> buttonLeave.setDisable(true));
                    break;
                case ROUND_OVER:
                    Platform.runLater(() -> buttonLeave.setDisable(false));
                    break;
            }
        });

        buttonLeave.setDisable(!bj.isRoundOver());

        this.setOurTurn(!bj.isRoundOver() && bj.getCurrentPlayer() == ourPlayer && bj.canHit());
    }

    private void setSecondRowShown(boolean on) {
        this.secondSet.setVisible(on);
        this.secondSet.setDisable(!on);
    }

    private void cleanup() {
        Player opj = this.playerInstance == null ? null : this.playerInstance.get();
        if (opj != null) opj.removeObserver(this);
    }

    @Override
    public void notify(Player instance, UpdateType<Player> type) {
        PlayerUpdate uType = (PlayerUpdate) type;
        Blackjack bj = this.gameInstance.get();
        if (bj != null) {
            List<Card> pCards = instance.getHand().getContent();
            switch (uType) {
                case PICK:
                    Platform.runLater(() -> {
                        if (!bj.isRoundOver() && bj.getCurrentPlayer() == instance)
                            this.buttonHit.setDisable(!bj.canHit());
                        for (int i = 0; i < pCards.size(); i++) this.cards.get(i).setCard(pCards.get(i));
                        if (pCards.size() > 4) this.setSecondRowShown(true);
                    });
                    break;
                case BET:
                    Platform.runLater(() -> this.fieldMoney.textProperty().setValue(String.valueOf(instance.getMoney())));
                    break;
                case EARN:
                    Platform.runLater(() -> this.fieldMoney.textProperty().setValue(String.valueOf(instance.getMoney())));
                    break;
                case TURN_START:
                    Platform.runLater(() -> this.setOurTurn(true));
                    break;
                case TURN_END:
                    Platform.runLater(() -> this.setOurTurn(false));
                    break;
            }
        }

    }

    @FXML
    private void leave(ActionEvent actionEvent) {
        Blackjack bj = this.gameInstance.get();
        Player player = this.playerInstance.get();
        if (bj != null && player != null) bj.leave(player);
        ((Stage) ((Button) actionEvent.getSource()).getScene().getWindow()).close();
    }
}
