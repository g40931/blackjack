package esi.atl.g40931.blackjack.controller;

import esi.atl.g40931.blackjack.view.HistoryView;
import esi.atl.g40931.blackjack.view.MainMenu;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class MainMenuController {

    @FXML
    private void about(ActionEvent event) {
        System.out.println("About");
        //TODO: implement
    }

    @FXML
    private void history(ActionEvent event) {
        new HistoryView().show();
    }

    @FXML
    private void newGame(ActionEvent event) {
        ((Stage) ((Node) event.getSource()).getScene().getWindow()).setScene(MainMenu.newGame);
    }

    @FXML
    private void initialize() {
        try {
            Class.forName("org.sqlite.SQLiteConfig");
        } catch (ClassNotFoundException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("SQLite driver not found!");
            alert.setContentText("The SQLite driver couldn't be loaded.\nEither it isn't on the classpath, or not with the correct version.\nUsing the persistence features will NOT work.");
            alert.getDialogPane().getStylesheets().add("/menus/menu.css");
            alert.showAndWait();
        }
    }
}
