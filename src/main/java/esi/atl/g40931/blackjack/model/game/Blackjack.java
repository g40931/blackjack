package esi.atl.g40931.blackjack.model.game;

import esi.atl.g40931.blackjack.model.players.Player;
import esi.atl.g40931.blackjack.mvc.Observable;

import java.util.List;

/**
 * Part of blackjack
 * <p>
 * Holds and handles all the logical aspects of the game.
 *
 * @author G40931
 */
public interface Blackjack extends Observable<Blackjack> {

    /**
     * Gets the list of players participating in this Blackjack game.
     * The returned List will still be used by this Blackjack.
     * Handle with care!
     *
     * @return players in this game
     */
    List<Player> getPlayers();

    /**
     * Gets the reference to the Bank player.
     * He is not forced to be of {@link esi.atl.g40931.blackjack.model.players.PlayerType#BANK} type.
     *
     * @return the bank player
     */
    Player getBank();

    /**
     * Gets the player who's turn it is.
     *
     * @return current player
     */
    Player getCurrentPlayer();

    /**
     * Returns whether the current player can hit.
     *
     * @return whether the current player can hit
     */
    boolean canHit();

    /**
     * Makes the current player hit (pick a card from the deck).
     */
    void hit();

    /**
     * Makes the current player stand (finish his turn).
     */
    void stand();

    /**
     * Gets the amount of played rounds on this game.
     *
     * @return amount of rounds played for this game
     */
    int getRounds();

    /**
     * Gets whether the current round is over.
     *
     * @return is the current round over?
     */
    boolean isRoundOver();

    /**
     * Advances to the next round.
     * Should only be used when the round is over.
     */
    void nextRound();

    /**
     * Gets the list of players that just won a round.
     *
     * @return players that just won a round
     */
    List<Player> getRoundWinners();

    /**
     * Marks the game as stopped, aborting the current thread.
     */
    void terminate();

    /**
     * Makes the player leave the game.
     *
     * @param player player to quit
     */
    void leave(Player player);
}
