/**
 * Part of blackjack.
 * <p>
 * Contains the model of the app.
 * <p>
 * Classes in the impl sub-package contain the implementation of the public interfaces in this package.
 *
 * @author G40931
 */
package esi.atl.g40931.blackjack.model;
