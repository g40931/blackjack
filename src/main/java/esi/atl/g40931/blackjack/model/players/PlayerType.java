package esi.atl.g40931.blackjack.model.players;

/**
 * Part of blackjack.
 *
 * @author G40931
 */
public enum PlayerType {
    BANK,
    REGULAR
}
