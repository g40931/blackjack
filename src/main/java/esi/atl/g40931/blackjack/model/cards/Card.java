package esi.atl.g40931.blackjack.model.cards;

import esi.atl.g40931.blackjack.mvc.Observable;

/**
 * Part of blackjack.
 * <p>
 * Represents a game card.
 *
 * @author G40931
 */
public interface Card extends Observable<Card> {

    /**
     * Returns the color of this card.
     *
     * @return the color of this card
     */
    Color getColor();

    /**
     * Returns the figure of this card.
     *
     * @return the figure of this card
     */
    Figure getFigure();

    /**
     * Returns the number of this card in a new deck.
     *
     * @return the number of this card in a new deck
     */
    int getNumber();

    /**
     * Returns whether this card is visible (can be seen by the players)
     *
     * @return the visibility of this card
     */
    boolean isVisible();

    /**
     * Sets this card to be visible (can be seen by the players)
     */
    void show();
}
