package esi.atl.g40931.blackjack.model.impl.players;

import esi.atl.g40931.blackjack.model.cards.Card;
import esi.atl.g40931.blackjack.model.cards.Deck;
import esi.atl.g40931.blackjack.model.players.Hand;
import esi.atl.g40931.blackjack.model.players.PlayerType;
import esi.atl.g40931.blackjack.mvc.ObservableHandler;
import esi.atl.g40931.blackjack.mvc.PlayerUpdate;

/**
 * Part of blackjack.
 *
 * @author G40931
 */
public class Player extends ObservableHandler<esi.atl.g40931.blackjack.model.players.Player> implements esi.atl.g40931.blackjack.model.players.Player {

    private final PlayerType type;
    private final Hand hand;
    private final String name;
    private final int initialMoney;
    private int money;

    public Player(PlayerType type) {
        this(type, null, 1000);
    }

    public Player(PlayerType type, String name, int money) {
        this.type = type;
        this.hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        this.name = name == null ? type.name().toLowerCase() : name;
        this.initialMoney = this.money = money;
    }

    @Override
    public Hand getHand() {
        return this.hand;
    }

    @Override
    public void pick(Deck from) {
        Card c = from.pick();
        if (this.hand.getContent().size() > 0 || !this.type.equals(PlayerType.BANK)) c.show();
        this.hand.addCard(c);
        this.notifyObservers(PlayerUpdate.PICK);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void bet(int amount) {
        if (this.type.equals(PlayerType.BANK)) throw new IllegalStateException("Tried to make the bank bet!");
        this.money -= amount;
        this.notifyObservers(PlayerUpdate.BET);
    }

    @Override
    public int getMoney() {
        return this.money;
    }

    @Override
    public int getInitialMoney() {
        return initialMoney;
    }

    @Override
    public PlayerType getPlayerType() {
        return this.type;
    }

    @Override
    public void earn(int amount) {
        if (this.type.equals(PlayerType.BANK)) throw new IllegalStateException("Tried to make the bank earn!");
        this.money += amount;
        this.notifyObservers(PlayerUpdate.EARN);
    }

    @Override
    public boolean isBusted() {
        return this.hand.getBest() > 21;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Player)) return false;

        Player player = (Player) o;

        if (type != player.type) return false;
        return getHand() != null ? getHand().equals(player.getHand()) : player.getHand() == null;

    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (getHand() != null ? getHand().hashCode() : 0);
        return result;
    }
}
