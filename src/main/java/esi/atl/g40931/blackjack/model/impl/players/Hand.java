package esi.atl.g40931.blackjack.model.impl.players;

import esi.atl.g40931.blackjack.model.cards.Card;
import esi.atl.g40931.blackjack.model.cards.Figure;
import esi.atl.g40931.blackjack.mvc.HandUpdate;
import esi.atl.g40931.blackjack.mvc.ObservableHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Part of blackjack.
 *
 * @author G40931
 */
public class Hand extends ObservableHandler<esi.atl.g40931.blackjack.model.players.Hand> implements esi.atl.g40931.blackjack.model.players.Hand {

    private List<Card> content = new ArrayList<>(2);

    private int[] valuesCache;

    @Override
    public int[] getPossibleValues() {
        if (this.valuesCache != null) return this.valuesCache;

        int lowestValue = this.content.stream().mapToInt(c -> c.getFigure().getValue()[0]).sum();

        int aces = (int) this.content.stream().filter(c -> c.getFigure().equals(Figure.ACE)).count();
        // Of course, this would break if other cards get multiple values. CBA handling it in a more expandable way.

        int[] values = new int[aces + 1];
        for (int i = 0; i < values.length; i++)
            values[i] = lowestValue - i * Figure.ACE.getValue()[0] + i * Figure.ACE.getValue()[1];

        int countNOOB = 0;
        for (int v : values) {
            if (v > 21) break;
            ++countNOOB;
        }

        this.valuesCache = new int[countNOOB < 1 ? 1 : countNOOB];
        System.arraycopy(values, 0, this.valuesCache, 0, countNOOB == 0 ? 1 : countNOOB);

        return this.valuesCache;
    }

    @Override
    public List<Card> getContent() {
        return this.content;
    }

    @Override
    public void showCard(int position) {
        if (position < 0 || position >= this.content.size())
            throw new ArrayIndexOutOfBoundsException("Tried to show a card at a position not in this hand! Provided position : " + position);
        this.content.get(position).show();
    }

    @Override
    public void addCard(Card card) {
        if (this.isFull()) throw new IllegalStateException("Tried to add a card to a full hand.");
        this.content.add(card);
        this.setChangeed();
        this.notifyObservers(HandUpdate.ADD_CARD);
    }

    @Override
    public boolean isFull() {
        return this.content.size() >= 7;
    }

    @Override
    public int getBest() {
        int[] t = this.getPossibleValues();
        return t[t.length - 1];
    }

    @Override
    public void clear() {
        this.content.clear();
        this.notifyObservers(HandUpdate.CLEAR);
    }

    private void setChangeed() {
        this.valuesCache = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hand)) return false;

        Hand hand = (Hand) o;

        return getContent() != null ? getContent().equals(hand.getContent()) : hand.getContent() == null;

    }

    @Override
    public int hashCode() {
        return getContent() != null ? getContent().hashCode() : 0;
    }
}
