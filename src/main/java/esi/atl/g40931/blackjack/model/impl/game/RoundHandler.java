package esi.atl.g40931.blackjack.model.impl.game;

import esi.atl.g40931.blackjack.model.players.Player;
import esi.atl.g40931.blackjack.model.players.PlayerType;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Part of blackjack
 * <p>
 * Handles all the round handling in a blackjack game : which player is to play, what round the game is at, ...
 *
 * @author G40931
 */
class RoundHandler {

    private final List<Player> players;
    private int currentPlayer = -1;
    private int roundCount = 0;

    RoundHandler(List<Player> players) {
        this.players = players;
    }

    List<Player> getPlayers() {
        return players;
    }

    int getRoundCount() {
        return roundCount;
    }

    Player getCurrentPlayer() {
        if (isRoundOver()) throw new IllegalStateException("Tried to get the current player while the round was over");
        return this.players.get(currentPlayer);
    }

    Player getBank() {
        return this.players.get(0);
    }

    boolean isRoundOver() {
        return this.currentPlayer == -1;
    }

    void increment() {
        this.currentPlayer = this.currentPlayer == 0 ? -1 : ++this.currentPlayer % players.size();
        if (this.currentPlayer == -1) ++roundCount;
    }

    private List<Player> getUnbusted() {
        return this.players.stream().filter(p -> !p.isBusted()).collect(Collectors.toList());
    }

    List<Player> getWinners() {
        return this.getBank().isBusted() ? this.getUnbusted() :
                this.getUnbusted().stream().filter(p -> p.getHand().getBest() > this.getBank().getHand().getBest())
                        .collect(Collectors.toList());
    }

    void nextRound(int bet) {
        this.players.forEach(p -> p.getHand().clear());
        this.currentPlayer = 1;
        this.players.stream().filter(p -> !p.getPlayerType().equals(PlayerType.BANK)).forEach(p -> p.bet(bet));
    }
}
