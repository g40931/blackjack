package esi.atl.g40931.blackjack.model.cards;

import esi.atl.g40931.blackjack.mvc.Observable;

/**
 * Part of blackjack.
 * <p>
 * Represents deck containing cards (initially 52).
 *
 * @author G40931
 */
public interface Deck extends Observable<Deck> {

    /**
     * Picks a card in this deck, effectively removing it from the deck.
     *
     * @return the picked card
     */
    Card pick();

    /**
     * Shuffles this deck with provided complexity.
     * The complexity is the amount of random swaps to use.
     *
     * @param complexity complexity to shuffle with
     */
    void shuffle(int complexity);

    /**
     * Gets whether this deck is empty.
     *
     * @return the emptyness of this deck
     */
    boolean isEmpty();
}
