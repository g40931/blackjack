package esi.atl.g40931.blackjack.model.cards;

/**
 * Part of blackjack.
 * <p>
 * Represents the four possible colors of a card.
 *
 * @author G40931
 */
public enum Color {
    HEART,
    CLUB,
    DIAMOND,
    SPADE;

    /**
     * Gets the appropriate color for a card, based on it's number.
     *
     * @param card the card to get the color of
     * @return the color of the card
     */
    public static Color of(Card card) {
        return values()[card.getNumber() / 13];
    }
}
