package esi.atl.g40931.blackjack.model.game;

import esi.atl.g40931.blackjack.model.players.Player;
import esi.atl.g40931.blackjack.model.players.PlayerType;

import java.util.ArrayList;
import java.util.List;

/**
 * Part of blackjack
 * <p>
 * Public way to "build" a Blackjack instance.
 * <p>
 * The methods in this class return the instance, to allow for a chained call :
 * <tt>Blackjack game = new BlackJackBuilder().addPlayer(PlayerType.BANK).addPlayer(PlayerType.REGULAR).setBet(25).generate();</tt>
 *
 * @author G40931
 */
public class BlackJackBuilder {
    private List<Player> players = new ArrayList<>(2);
    private int bet = 0;

    /**
     * Adds a player to this builder based on a {@link PlayerType}.
     *
     * @param type the type of player to add
     * @return the instance of this builder
     */
    public BlackJackBuilder addPlayer(PlayerType type) {
        return this.addPlayer(type, null, 1000);
    }

    /**
     * Adds a player to this builder based on a {@link PlayerType}, a name and an initial money amount.
     *
     * @param type  the type of player to add
     * @param name  name for the player
     * @param money initial money of the player
     * @return the instance of this builder
     */
    public BlackJackBuilder addPlayer(PlayerType type, String name, int money) {
        return this.addPlayer(new esi.atl.g40931.blackjack.model.impl.players.Player(type, name, money));
    }

    /**
     * Adds a player to this builder.
     *
     * @param player the player to add
     * @return the instance of this builder
     */
    public BlackJackBuilder addPlayer(Player player) {
        this.players.add(player);
        return this;
    }

    /**
     * Sets the bet amount for this builder.
     *
     * @param value new value for the bet
     * @return the instance of this builder
     */
    public BlackJackBuilder setBet(int value) {
        this.bet = value;
        return this;
    }

    /**
     * Creates a new Blackjack instance based on all data previously set.
     *
     * @return new blackjack instance configured with previously set data
     */
    public Blackjack build() {
        if (this.players.size() < 2)
            throw new IllegalStateException("Can't build a Blackjack instance with less than 2 players!");
        return new esi.atl.g40931.blackjack.model.impl.game.Blackjack(this.players, this.bet);
    }
}
