package esi.atl.g40931.blackjack.model.cards;

/**
 * Part of blackjack.
 * <p>
 * Represents the 13 possible figures of a card.
 *
 * @author G40931
 */
public enum Figure {
    ACE(1, 11),
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK(10),
    QUEEN(10),
    KING(10);

    private final int[] value;

    Figure() {
        this.value = new int[]{this.ordinal() + 1};
    }

    Figure(int... value) {
        this.value = value;
    }

    /**
     * Gets the appropriate figure for a card, based on it's number.
     *
     * @param card the card to get the figure of
     * @return the figure of the card
     */
    public static Figure of(Card card) {
        return values()[card.getNumber() % 13];
    }

    /**
     * Gets the possible values for this card figure.
     *
     * @return possible values for this card figure
     */
    public int[] getValue() {
        return value;
    }
}
