package esi.atl.g40931.blackjack.model.impl.cards;

import esi.atl.g40931.blackjack.model.cards.Color;
import esi.atl.g40931.blackjack.model.cards.Figure;
import esi.atl.g40931.blackjack.mvc.CardUpdate;
import esi.atl.g40931.blackjack.mvc.ObservableHandler;

/**
 * Part of blackjack.
 *
 * @author G40931
 */
public class Card extends ObservableHandler<esi.atl.g40931.blackjack.model.cards.Card> implements esi.atl.g40931.blackjack.model.cards.Card {
    private final int number;
    private boolean visible = false;

    public Card(int number) {
        if (number < 0 || number >= 52)
            throw new IllegalArgumentException("A card's number must be between 0 (inclusive) and 52 (exclusive). Provided: " + number);
        this.number = number;
    }

    @Override
    public Color getColor() {
        return Color.of(this);
    }

    @Override
    public Figure getFigure() {
        return Figure.of(this);
    }

    @Override
    public int getNumber() {
        return this.number;
    }

    @Override
    public boolean isVisible() {
        return this.visible;
    }

    @Override
    public void show() {
        this.visible = true;
        this.notifyObservers(CardUpdate.FLIP);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Card)) return false;

        Card card = (Card) o;

        return getNumber() == card.getNumber();

    }

    @Override
    public int hashCode() {
        return getNumber();
    }

    @Override
    public String toString() {
        return "Card{number=" + number + "} = " + this.getFigure().toString().toLowerCase() + " of " + this.getColor().toString().toLowerCase() + "s";
    }
}
