package esi.atl.g40931.blackjack.model.impl.game;

import esi.atl.g40931.blackjack.model.cards.Deck;
import esi.atl.g40931.blackjack.model.players.Player;
import esi.atl.g40931.blackjack.mvc.BlackjackUpdate;
import esi.atl.g40931.blackjack.mvc.ObservableThreadHandler;
import esi.atl.g40931.blackjack.mvc.PlayerUpdate;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class Blackjack extends ObservableThreadHandler<esi.atl.g40931.blackjack.model.game.Blackjack> implements esi.atl.g40931.blackjack.model.game.Blackjack {

    private final int bet;
    private final List<Runnable> actions = Collections.synchronizedList(new LinkedList<>());
    private final RoundHandler round;
    private Deck deck;
    private List<Player> roundWinners;
    private boolean stop = false;

    /**
     * Creates a new Blackjack instance.
     * <p>
     * The provided player list should contain at least 2 players, and the first player should be the bank
     * (but not forced to be of {@link esi.atl.g40931.blackjack.model.players.PlayerType#BANK} type).
     * <p>
     * The bet amount fixes the amount of money players pay to play each round and receive when winning.
     *
     * @param players list of players to be used in this game
     * @param bet     amount of money players pay to play each round and receive when winning
     */
    public Blackjack(List<Player> players, int bet) {
        if (players.size() < 2)
            throw new IllegalStateException("Provided player list too small. Size: " + players.size() + ". Minimum allowed: 2.");
        this.round = new RoundHandler(players);
        this.bet = bet;
        this.setDaemon(true);
        this.setName("Blackjack game thread");
        this.start();
    }

    @Override
    public void run() {
        while (!stop) {
            while (!stop && this.actions.size() > 0) this.actions.remove(0).run();
            if (!stop) try {
                synchronized (this.actions) {
                    this.actions.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<Player> getPlayers() {
        return this.round.getPlayers();
    }

    @Override
    public Player getBank() {
        return this.round.getBank();
    }

    @Override
    public Player getCurrentPlayer() {
        return this.round.getCurrentPlayer();
    }

    @Override
    public boolean canHit() {
        return this.getCurrentPlayer().getHand().getBest() < 21; // One cannot hit with a score of 21 or more
    }

    @Override
    public void hit() {
        if (isRoundOver()) throw new IllegalStateException("Tried to hit while the round was over");
        if (!canHit()) throw new IllegalStateException("Tried to hit when not allowed to");
        this.addAction(() -> {
            if (isRoundOver()) throw new IllegalStateException("Tried to hit while the round was over");
            if (!canHit()) throw new IllegalStateException("Tried to hit when not allowed to");
            this.getCurrentPlayer().pick(this.deck);
            this.notifyObservers(BlackjackUpdate.HIT);
            synchronized (this) {
                this.notify();
            }
        });
    }

    @Override
    public void stand() {
        if (isRoundOver()) throw new IllegalStateException("Tried to stand while the round was over");
        this.addAction(() -> {
            if (isRoundOver()) throw new IllegalStateException("Tried to stand while the round was over");
            this.notifyObservers(BlackjackUpdate.STAND);
            this.getCurrentPlayer().notifyObservers(PlayerUpdate.TURN_END);
            this.round.increment();
            if (isRoundOver()) this.checkWinners();
            else this.getCurrentPlayer().notifyObservers(PlayerUpdate.TURN_START);
            synchronized (this) {
                this.notify();
            }
        });
    }

    private void checkWinners() {
        List<Player> winners = this.round.getWinners();

        winners.forEach(p -> p.earn(this.bet * 2));
        this.roundWinners = winners;
        this.notifyObservers(BlackjackUpdate.ROUND_OVER);
    }

    @Override
    public int getRounds() {
        return this.round.getRoundCount();
    }

    @Override
    public boolean isRoundOver() {
        return this.round.isRoundOver();
    }

    @Override
    public void nextRound() {
        if (!isRoundOver()) throw new IllegalStateException("Tried to go to next round before the round was over");
        this.addAction(() -> {
            if (!isRoundOver()) throw new IllegalStateException("Tried to go to next round before the round was over");
            this.notifyObservers(BlackjackUpdate.NEXT_ROUND_PRE);
            this.roundWinners = null;
            this.round.nextRound(this.bet);
            this.deck = new esi.atl.g40931.blackjack.model.impl.cards.Deck();
            this.deck.shuffle(150);
            for (int i = 0; i < 2; i++) this.getPlayers().forEach(p -> p.pick(this.deck));
            synchronized (this) {
                this.notify();
            }
            this.notifyObservers(BlackjackUpdate.NEXT_ROUND_POST);
            this.getCurrentPlayer().notifyObservers(PlayerUpdate.TURN_START);
        });
    }

    private void addAction(Runnable r) {
        synchronized (this.actions) {
            this.actions.add(r);
            this.actions.notify();
        }
    }

    @Override
    public List<Player> getRoundWinners() {
        if (!this.isRoundOver()) throw new IllegalStateException("Current round isn't over!");
        return this.roundWinners;
    }

    @Override
    public void terminate() {
        this.stop = true;
        synchronized (this.actions) {
            this.actions.notify();
        }
    }

    @Override
    public void leave(Player player) {
        this.round.getPlayers().remove(player);
    }
}
