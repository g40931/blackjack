package esi.atl.g40931.blackjack.model.players;

import esi.atl.g40931.blackjack.model.cards.Deck;
import esi.atl.g40931.blackjack.mvc.Observable;

/**
 * Part of blackjack.
 *
 * @author G40931
 */
public interface Player extends Observable<Player> {

    /**
     * Gets this player's hand.
     *
     * @return the hand this player has
     */
    Hand getHand();

    /**
     * Picks a card from the provided {@link Deck} and adds it to this player's hand.
     *
     * @param from the deck to pick from
     */
    void pick(Deck from);

    /**
     * Gets the name set for this player.
     *
     * @return name of this player
     */
    String getName();

    /**
     * Makes this player place a bet.
     *
     * @param amount the amount to bet
     */
    void bet(int amount);

    /**
     * Gets the current money of the player.
     *
     * @return the money this player has
     */
    int getMoney();

    /**
     * Gets the amount of money the player had when starting the game.
     *
     * @return initial amount of money of this player
     */
    int getInitialMoney();

    /**
     * Gets the type of player this references.
     *
     * @return the type of this player
     */
    PlayerType getPlayerType();

    /**
     * Make this player earn money (usually by winning...)
     *
     * @param amount the amount to grant to this player
     */
    void earn(int amount);

    /**
     * Gets whether the player is busted or not.
     *
     * @return bustedness of the player
     */
    boolean isBusted();
}
