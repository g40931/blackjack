package esi.atl.g40931.blackjack.model.impl.cards;

import esi.atl.g40931.blackjack.mvc.DeckUpdate;
import esi.atl.g40931.blackjack.mvc.ObservableHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Part of blackjack.
 *
 * @author G40931
 */
public class Deck extends ObservableHandler<esi.atl.g40931.blackjack.model.cards.Deck> implements esi.atl.g40931.blackjack.model.cards.Deck {
    private List<esi.atl.g40931.blackjack.model.cards.Card> cards;

    public Deck() {
        this.cards = new ArrayList<>(52);
        for (int i = 0; i < 52; i++) this.cards.add(new Card(i));
    }

    @Override
    public esi.atl.g40931.blackjack.model.cards.Card pick() {
        if (this.isEmpty()) throw new IllegalStateException("Tried to pick from an empty deck!");
        esi.atl.g40931.blackjack.model.cards.Card c = this.cards.remove(this.cards.size() - 1);
        this.notifyObservers(DeckUpdate.PICK);
        return c;
    }

    @Override
    public void shuffle(int complexity) {
        Random rng = new Random();
        for (int i = 0; i < complexity; i++) swap(rng.nextInt(52), rng.nextInt(52));
        this.notifyObservers(DeckUpdate.SHUFFLE);
    }

    @Override
    public boolean isEmpty() {
        return this.cards.isEmpty();
    }

    private void swap(int a, int b) {
        if (a != b) this.cards.set(b, this.cards.set(a, this.cards.get(b)));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Deck)) return false;

        Deck deck = (Deck) o;

        return cards != null ? cards.equals(deck.cards) : deck.cards == null;
    }

    @Override
    public int hashCode() {
        return cards != null ? cards.hashCode() : 0;
    }
}
