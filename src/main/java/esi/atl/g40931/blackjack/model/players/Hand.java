package esi.atl.g40931.blackjack.model.players;

import esi.atl.g40931.blackjack.model.cards.Card;
import esi.atl.g40931.blackjack.mvc.Observable;

import java.util.List;

/**
 * Part of blackjack.
 *
 * @author G40931
 */
public interface Hand extends Observable<Hand> {

    /**
     * Gets the possible score values for this hand.
     * Only contains multiple values in the case of an {@link esi.atl.g40931.blackjack.model.cards.Figure#ACE}.
     * In case of one or more of the multiple values exceeding 21, only values below 21 (or the lowest possible value)
     * will be returned.
     * All values are in ascending order.
     *
     * @return possible score values for this hand
     */
    int[] getPossibleValues();

    /**
     * Gets the cards in this hand.
     * The returned List will still be used by this hand.
     * Handle with care!
     *
     * @return the cards in this hand
     */
    List<Card> getContent();

    /**
     * Sets a card to be visible, based on it's position in this hand.
     *
     * @param position the position of the card to show
     */
    void showCard(int position);

    /**
     * Adds a card to this hand.
     *
     * @param card the card to add
     */
    void addCard(Card card);

    /**
     * Gets whether this hand is full or not.
     * A hand is considered full when it contains the maximum of 7 cards.
     *
     * @return whether this hand is full
     */
    boolean isFull();

    /**
     * Gets the best score for this hand.
     *
     * @return best score for this hand
     */
    int getBest();

    /**
     * Clear this hand of it's content.
     */
    void clear();
}
