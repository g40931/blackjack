package esi.atl.g40931.blackjack.data;

import org.sqlite.SQLiteConfig;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class DBManager {

    /**
     *
     */
    public static final DBManager INSTANCE = new DBManager();

    private static final String URI = "jdbc:sqlite:savedata.bj";

    private final Connection CONNECTION;

    private DBManager() {
        Connection c = null;
        try {
            SQLiteConfig config = new SQLiteConfig();
            config.enforceForeignKeys(true); // Foreign keys won't be respected without this :x
            c = DriverManager.getConnection(URI, config.toProperties());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        CONNECTION = c;

        setup();
    }

    private void setup() {
        try (Statement s = CONNECTION.createStatement()) {
            s.executeUpdate("CREATE TABLE IF NOT EXISTS games(id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(50) DEFAULT NULL, time DATE DEFAULT NULL, rounds INTEGER, earnings INTEGER, UNIQUE (name, time))");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the history for given player.
     * Equivalent to {@code getHistory(null)}.
     *
     * @return history of provided player
     */
    public List<ResultDTO> getHistory() {
        return this.getHistory("%");
    }

    /**
     * Gets the history for given player.
     *
     * @param player player name (% acting as wildcard)
     * @return history of provided player
     */
    public List<ResultDTO> getHistory(String player) {
        List<ResultDTO> history = new ArrayList<>();
        player = player == null || player.length() == 0 ? "%" : "%" + player + "%";

        try (PreparedStatement s = CONNECTION.prepareStatement("SELECT name, time, rounds, earnings FROM games WHERE name LIKE ?")) {
            s.setString(1, player);
            ResultSet rs = s.executeQuery();
            if (!rs.isClosed()) while (rs.next()) history.add(new ResultDTO(
                    rs.getString("name"),
                    rs.getDate("time").toLocalDate(),
                    rs.getInt("rounds"),
                    rs.getInt("earnings")
            ));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return history;
    }

    /**
     * Saves a game result.
     *
     * @param game result to save
     */
    public void save(ResultDTO game) {
        try (PreparedStatement s = CONNECTION.prepareStatement("INSERT INTO games (name, time, rounds, earnings) VALUES (?, ?, ?, ?)")) {
            s.setString(1, game.getPlayerName());
            s.setDate(2, Date.valueOf(game.getTime()));
            s.setInt(3, game.getRoundsPlayed());
            s.setInt(4, game.getEarnings());
            s.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
