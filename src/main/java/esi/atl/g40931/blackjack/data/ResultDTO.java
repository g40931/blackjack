package esi.atl.g40931.blackjack.data;

import esi.atl.g40931.blackjack.model.players.Player;

import java.time.LocalDate;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class ResultDTO {

    private final String playerName;
    private final LocalDate time;
    private final int roundsPlayed;
    private final int earnings;

    public ResultDTO(String playerName, LocalDate time, int roundsPlayed, int earnings) {
        this.playerName = playerName;
        this.time = time;
        this.roundsPlayed = roundsPlayed;
        this.earnings = earnings;
    }

    public ResultDTO(Player player, int rounds) {
        this(player.getName(), LocalDate.now(), rounds, player.getMoney() - player.getInitialMoney());
    }

    public String getPlayerName() {
        return playerName;
    }

    public LocalDate getTime() {
        return time;
    }

    public int getRoundsPlayed() {
        return roundsPlayed;
    }

    public int getEarnings() {
        return earnings;
    }
}
