package esi.atl.g40931.blackjack.mvc;

import esi.atl.g40931.blackjack.model.cards.Card;

/**
 * Part of blackjack
 * <p>
 * Defines possible updates for Cards.
 *
 * @author G40931
 */
public enum CardUpdate implements UpdateType<Card> {
    /**
     * Sent when a card is flipped.
     */
    FLIP
}
