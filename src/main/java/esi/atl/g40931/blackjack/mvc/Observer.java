package esi.atl.g40931.blackjack.mvc;

/**
 * Part of blackjack
 * <p>
 * Marks a class as observer of an {@link Observable}
 *
 * @author G40931
 */
public interface Observer<T> {

    /**
     * Notifies an observer of an update of the observable.
     *
     * @param instance changed instance
     * @param type     type of update
     */
    void notify(T instance, UpdateType<T> type);
}
