package esi.atl.g40931.blackjack.mvc;

import java.util.ArrayList;
import java.util.List;

/**
 * Part of blackjack
 * <p>
 * Implementation for the observable interface.
 * The generic type should be extended by the class extending this.
 *
 * @author G40931
 */
public abstract class ObservableHandler<T> implements Observable<T> {

    private final List<Observer<T>> observers = new ArrayList<>();

    @Override
    public final void registerObserver(Observer<T> observer) {
        this.observers.add(observer);
    }

    @Override
    public final void removeObserver(Observer<T> observer) {
        this.observers.remove(observer);
    }

    @Override
    public final void notifyObservers(UpdateType<T> type) {
        //noinspection unchecked
        this.observers.forEach(o -> o.notify((T) this, type));
    }
}
