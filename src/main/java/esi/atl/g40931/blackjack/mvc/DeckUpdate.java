package esi.atl.g40931.blackjack.mvc;

import esi.atl.g40931.blackjack.model.cards.Deck;

/**
 * Part of blackjack
 * <p>
 * Defines possible updates for Decks.
 *
 * @author G40931
 */
public enum DeckUpdate implements UpdateType<Deck> {

    /**
     * Sent when the deck is shuffled.
     */
    SHUFFLE,

    /**
     * Sent when a card is picked from the deck.
     */
    PICK
}
