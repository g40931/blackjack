package esi.atl.g40931.blackjack.mvc;

/**
 * Part of blackjack
 * <p>
 * Instances of this interface denote changes in the model.
 *
 * @author G40931
 */
public interface UpdateType<T> {
}
