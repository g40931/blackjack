package esi.atl.g40931.blackjack.mvc;

/**
 * Part of blackjack
 * <p>
 * Marks a class as observable by {@link Observer}
 *
 * @author G40931
 */
public interface Observable<T> {

    /**
     * Registers an observer to be notified of all updates of this observable.
     *
     * @param observer the observer to register
     */
    void registerObserver(Observer<T> observer);

    /**
     * Removes a previously registered observer.
     * Silently ignores non-registered observers.
     *
     * @param observer the observer to remove
     */
    void removeObserver(Observer<T> observer);

    /**
     * Notifies all registered observers of an update.
     *
     * @param type the type of update
     */
    void notifyObservers(UpdateType<T> type);
}
