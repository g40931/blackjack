package esi.atl.g40931.blackjack.mvc;

import esi.atl.g40931.blackjack.model.game.Blackjack;

/**
 * Part of blackjack
 * <p>
 * Defines possible updates for Blackjack.
 *
 * @author G40931
 */
public enum BlackjackUpdate implements UpdateType<Blackjack> {

    /**
     * Sent when the current player hits.
     */
    HIT,

    /**
     * Sent when the current player stands.
     */
    STAND,

    /**
     * Sent when the game advances to next round, before the new deck is created and the cards are picked.
     */
    NEXT_ROUND_PRE,

    /**
     * Sent when the game advances to next round, after the new deck is created and the cards are picked.
     */
    NEXT_ROUND_POST,

    /**
     * Sent when the current round is over and winners have been awarded their money.
     */
    ROUND_OVER
}
