package esi.atl.g40931.blackjack.mvc;

import esi.atl.g40931.blackjack.model.players.Player;

/**
 * Part of blackjack
 * <p>
 * Defines possible updates for Players.
 *
 * @author G40931
 */
public enum PlayerUpdate implements UpdateType<Player> {

    /**
     * Sent when the player picks a card.
     */
    PICK,

    /**
     * Sent when the player bets.
     */
    BET,

    /**
     * Sent when the player earns money.
     */
    EARN,

    /**
     * Sent when the player's turn starts.
     */
    TURN_START,

    /**
     * Sent when the player's turn ends.
     */
    TURN_END
}
