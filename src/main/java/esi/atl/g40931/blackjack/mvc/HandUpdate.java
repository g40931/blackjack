package esi.atl.g40931.blackjack.mvc;

import esi.atl.g40931.blackjack.model.players.Hand;

/**
 * Part of blackjack
 * <p>
 * Defines possible updates for Hands.
 *
 * @author G40931
 */
public enum HandUpdate implements UpdateType<Hand> {
    /**
     * Sent when a card is added to the hand.
     */
    ADD_CARD,

    /**
     * Sent when the hand is cleared.
     */
    CLEAR
}
