package esi.atl.g40931.blackjack.view;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class MainMenu extends Application {

    public static Scene mainMenu;
    public static Scene newGame;

    private static void initScenes() {
        try {
            mainMenu = new Scene(FXMLLoader.load(MainMenu.class.getResource("/menus/Main.fxml")));
            newGame = new Scene(FXMLLoader.load(MainMenu.class.getResource("/menus/NewGame.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
            Platform.exit();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        initScenes();

        primaryStage.setScene(mainMenu);
        primaryStage.setTitle("Blackjack");
        primaryStage.getIcons().add(new Image("icons/sora_badass.jpg"));
        primaryStage.show();
    }
}
