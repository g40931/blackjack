package esi.atl.g40931.blackjack.view;

import javafx.scene.control.TextField;

/**
 * Part LodeRunner.
 * Used for numbers input.
 * Only allows for positive numbers to be input.
 *
 * @author Arnaud Solé
 */
public class IntField extends TextField {

    /**
     * Creates a new number input field with given initial value.
     *
     * @param initialValue initial value to use
     */
    public IntField(int initialValue) {
        super(String.valueOf(initialValue));
    }

    /**
     * Creates a new empty number input field
     */
    public IntField() {
        super();
    }

    /**
     * @inheritDoc Only allows for numbers to be input.
     */
    @Override
    public void replaceText(int start, int end, String text) {
        if (validate(text)) super.replaceText(start, end, text);
    }

    /**
     * @inheritDoc Only allows for numbers to be input.
     */
    @Override
    public void replaceSelection(String text) {
        if (validate(text)) super.replaceSelection(text);
    }

    private boolean validate(String text) {
        return ("".equals(text) || text.matches("[0-9]"));
    }

    /**
     * Gets the int value written in this input.
     *
     * @return the int value in this input
     */
    public int getValue() {
        int v = 0;
        if (getText().length() > 0) try {
            v = Integer.parseInt(getText());
        } catch (NumberFormatException e) { // Only with too high/low numbers does this fail, non-numbers aren't allowed.
            v = Integer.MAX_VALUE;
        }
        return v;
    }
}
