package esi.atl.g40931.blackjack.view;

import esi.atl.g40931.blackjack.model.cards.Card;
import javafx.scene.image.Image;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class CardImage extends Image {

    public CardImage(Card card) { // 390 * 606
        super(String.format("cards/%s%s.png", card.getColor(), card.getFigure()));
    }
}
