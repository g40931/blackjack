package esi.atl.g40931.blackjack.view;

import javafx.scene.image.Image;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class HiddenCardImage extends Image {

    public static HiddenCardImage INSTANCE = new HiddenCardImage();

    private HiddenCardImage() { // 390 * 606
        super("cards/cardHidden.png");
    }
}
