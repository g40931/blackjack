package esi.atl.g40931.blackjack.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class HistoryView extends Stage {

    public HistoryView() {
        try {
            this.setScene(new Scene(FXMLLoader.load(MainMenu.class.getResource("/menus/History.fxml"))));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
