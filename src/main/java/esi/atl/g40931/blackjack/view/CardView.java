package esi.atl.g40931.blackjack.view;

import esi.atl.g40931.blackjack.model.cards.Card;
import esi.atl.g40931.blackjack.mvc.CardUpdate;
import esi.atl.g40931.blackjack.mvc.Observer;
import esi.atl.g40931.blackjack.mvc.UpdateType;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class CardView extends Rectangle implements Observer<Card> {

    public static final String STYLE_CLASS = "card-view";

    public CardView() {
        try {
            this.setFill(new ImagePattern(UnknownCardImage.INSTANCE));
        } catch (Exception ignored) {
        } // For loading in SceneBuilder
        getStyleClass().add(STYLE_CLASS);
    }

    public CardView(Card card) {
        this();
        this.setCard(card);
    }

    public void setCard(Card card) {
        card.registerObserver(this);
        this.setFill(new ImagePattern(card.isVisible() ? new CardImage(card) : HiddenCardImage.INSTANCE));
    }

    @Override
    public void notify(Card instance, UpdateType<Card> type) {
        CardUpdate c = ((CardUpdate) type);
        switch (c) {
            case FLIP:
                this.setFill(new ImagePattern(instance.isVisible() ? new CardImage(instance) : HiddenCardImage.INSTANCE));
                break;
        }
    }

    public void clear() {
        this.setFill(new ImagePattern(UnknownCardImage.INSTANCE));
    }
}
