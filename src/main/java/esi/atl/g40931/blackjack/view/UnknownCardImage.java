package esi.atl.g40931.blackjack.view;

import javafx.scene.image.Image;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class UnknownCardImage extends Image {

    public static UnknownCardImage INSTANCE = new UnknownCardImage();

    private UnknownCardImage() { // 390 * 606
        super("cards/cardEmpty.png");
    }
}
