package esi.atl.g40931.blackjack.view;

import esi.atl.g40931.blackjack.controller.HandViewController;
import esi.atl.g40931.blackjack.controller.RecapViewController;
import esi.atl.g40931.blackjack.model.game.Blackjack;
import esi.atl.g40931.blackjack.model.players.Player;
import esi.atl.g40931.blackjack.model.players.PlayerType;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class HandView extends Stage {

    private final WeakReference<Player> ourPlayer;
    private final WeakReference<Blackjack> bj;

    public HandView(Blackjack bj, Player ourPlayer) {
        this.bj = new WeakReference<>(bj);
        this.ourPlayer = new WeakReference<>(ourPlayer);

        try {
            FXMLLoader loader = new FXMLLoader();
            Parent root = loader.load(MainMenu.class.getResource("/menus/HandView.fxml").openStream());
            HandViewController controller = loader.getController();
            controller.initialize(bj, ourPlayer);
            this.setTitle(ourPlayer.getName() + " View");
            this.setScene(new Scene(root, 600, 442));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        Player player = this.ourPlayer.get();
        if (player != null && player.getPlayerType().equals(PlayerType.BANK)) super.close();
        else try {
            FXMLLoader loader = new FXMLLoader();
            Parent root = loader.load(MainMenu.class.getResource("/menus/RecapView.fxml").openStream());
            RecapViewController controller = loader.getController();
            Blackjack bj = this.bj.get();
            controller.init(player, bj == null ? 0 : bj.getRounds());
            this.setTitle(player == null ? "unknown" : player.getName() + " Recap");
            this.setScene(new Scene(root, 600, 442));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
