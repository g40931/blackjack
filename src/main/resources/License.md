_aka "who do the images belong to"_

SORA :
====
The sora_badass.jpg image used in various places for this project is part of the anime [No Game No Life](http://ngnl.jp/) by studio Madhouse, based on the light novel written by Yuu Kamiya and published by Kadokawa メディアファクトリー (Media Factory) in Japan.

Most images :
====
Released by [Game Icons](http://game-icons.net/) under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/), the ones I used were made by [Faithtoken](http://www.faithtoken.com/) and [Lorc](http://lorcblog.blogspot.be/).

Cards :
====
Found on [OpenGameArt](http://opengameart.org/content/playing-card-assets-52-cards-deck-chips), released under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).
