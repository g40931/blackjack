package esi.atl.g40931.blackjack.model.cards;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;

/**
 * Part of blackjack.
 *
 * @author G40931
 */
public class CardTest {

    @Test
    public void constructor() throws Exception {
        Field f = esi.atl.g40931.blackjack.model.impl.cards.Card.class.getDeclaredField("number");
        f.setAccessible(true);
        for (int i = 0; i < 52; i++)
            Assert.assertEquals(i, f.getInt(new esi.atl.g40931.blackjack.model.impl.cards.Card(i)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorOOBUp() throws Exception {
        new esi.atl.g40931.blackjack.model.impl.cards.Card(52);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorOOBDown() throws Exception {
        new esi.atl.g40931.blackjack.model.impl.cards.Card(-1);
    }

    @Test
    public void getColor() throws Exception {

        for (int i = 0; i < 13; i++) {
            Assert.assertEquals(Color.HEART, new esi.atl.g40931.blackjack.model.impl.cards.Card(i).getColor());

            Assert.assertEquals(Color.CLUB, new esi.atl.g40931.blackjack.model.impl.cards.Card(i + 13).getColor());

            Assert.assertEquals(Color.DIAMOND, new esi.atl.g40931.blackjack.model.impl.cards.Card(i + 26).getColor());

            Assert.assertEquals(Color.SPADE, new esi.atl.g40931.blackjack.model.impl.cards.Card(i + 39).getColor());
        }
    }

    @Test
    public void getFigure() throws Exception {
        for (int i = 0; i < 13; i++) {
            Assert.assertEquals(Figure.values()[i], new esi.atl.g40931.blackjack.model.impl.cards.Card(i).getFigure());

            Assert.assertEquals(Figure.values()[i], new esi.atl.g40931.blackjack.model.impl.cards.Card(i + 13).getFigure());

            Assert.assertEquals(Figure.values()[i], new esi.atl.g40931.blackjack.model.impl.cards.Card(i + 26).getFigure());

            Assert.assertEquals(Figure.values()[i], new esi.atl.g40931.blackjack.model.impl.cards.Card(i + 39).getFigure());
        }
    }

    @Test
    public void getNumber() throws Exception {
        Assert.assertEquals(12, new esi.atl.g40931.blackjack.model.impl.cards.Card(12).getNumber());
    }

    @Test
    public void equals() throws Exception {
        Assert.assertEquals(new esi.atl.g40931.blackjack.model.impl.cards.Card(1), new esi.atl.g40931.blackjack.model.impl.cards.Card(1));
    }

}
