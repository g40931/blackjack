package esi.atl.g40931.blackjack.model.game;

import esi.atl.g40931.blackjack.model.players.Player;
import esi.atl.g40931.blackjack.model.players.PlayerType;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class BlackJackBuilderTest {

    @Test(expected = IllegalStateException.class)
    public void buildISE() throws Exception {
        new BlackJackBuilder().build();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void build() throws Exception {
        Player p1 = new esi.atl.g40931.blackjack.model.impl.players.Player(PlayerType.BANK);
        Player p2 = new esi.atl.g40931.blackjack.model.impl.players.Player(PlayerType.REGULAR);
        int bet = 25;

        Blackjack bj = new BlackJackBuilder().addPlayer(p1).addPlayer(p2).setBet(bet).build(); // :wink:
        Field playersField = Class.forName("esi.atl.g40931.blackjack.model.impl.game.RoundHandler").getDeclaredField("players");
        Field betField = esi.atl.g40931.blackjack.model.impl.game.Blackjack.class.getDeclaredField("bet");
        Field roundField = esi.atl.g40931.blackjack.model.impl.game.Blackjack.class.getDeclaredField("round");
        playersField.setAccessible(true);
        betField.setAccessible(true);
        roundField.setAccessible(true);

        List<Player> players = (List<Player>) playersField.get(roundField.get(bj));
        Assert.assertEquals(p1, players.get(0));
        Assert.assertEquals(p2, players.get(1));

        Assert.assertEquals(bet, betField.getInt(bj));
    }

}
