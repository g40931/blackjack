package esi.atl.g40931.blackjack.model.players;

import esi.atl.g40931.blackjack.model.impl.cards.Card;
import esi.atl.g40931.blackjack.model.impl.cards.Deck;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Part of blackjack by Bluexin.
 *
 * @author Bluexin
 */
public class HandTest {

    /*
    Useful when looking for performance issues.
    This will somewhat warm the JVM up
    (in regards to streams for example, for which the first one tends to take ~50ms, the next ones being nice and fast).
     */
    /*static {
        HandTest ht = new HandTest();
        try {
            for (int i = 0; i < 100; i++) ht.getPossibleValuesTwoAceOneOOB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Test
    public void getPossibleValuesNoAce() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(1)); // Score 2
        hand.addCard(new Card(8)); // Score 9
        hand.addCard(new Card(11)); // Score 10
        Assert.assertArrayEquals(new int[]{21}, hand.getPossibleValues());
    }

    @Test
    public void getBestNoAce() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(1)); // Score 2
        hand.addCard(new Card(8)); // Score 9
        hand.addCard(new Card(11)); // Score 10
        Assert.assertEquals(21, hand.getBest());
    }

    @Test
    public void getPossibleValuesNoAceOOB() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(1)); // Score 2
        hand.addCard(new Card(9)); // Score 10
        hand.addCard(new Card(11)); // Score 10
        Assert.assertArrayEquals(new int[]{22}, hand.getPossibleValues());
    }

    @Test
    public void getBestNoAceOOB() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(1)); // Score 2
        hand.addCard(new Card(9)); // Score 10
        hand.addCard(new Card(11)); // Score 10
        Assert.assertEquals(22, hand.getBest());
    }

    @Test
    public void getPossibleValuesOneAceNonOOB() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(8)); // Score 9
        Assert.assertArrayEquals(new int[]{10, 20}, hand.getPossibleValues());
    }

    @Test
    public void getBestOneAceNonOOB() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(8)); // Score 9
        Assert.assertEquals(20, hand.getBest());
    }

    @Test
    public void getPossibleValuesOneAceOneOOB() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(9)); // Score 10
        hand.addCard(new Card(9)); // Score 10
        Assert.assertArrayEquals(new int[]{21}, hand.getPossibleValues());
    }

    @Test
    public void getBestOneAceOneOOB() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(9)); // Score 10
        hand.addCard(new Card(9)); // Score 10
        Assert.assertEquals(21, hand.getBest());
    }

    @Test
    public void getPossibleValuesTwoAceOneOOB() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(0)); // Score 1 or 11
        Assert.assertArrayEquals(new int[]{2, 12}, hand.getPossibleValues());
    }

    @Test
    public void getBestTwoAceOneOOB() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(0)); // Score 1 or 11
        Assert.assertEquals(12, hand.getBest());
    }

    @Test
    public void getPossibleValuesTwoAceTwoOOB() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(9)); // Score 10
        Assert.assertArrayEquals(new int[]{12}, hand.getPossibleValues());
    }

    @Test
    public void getBestTwoAceTwoOOB() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(9)); // Score 10
        Assert.assertEquals(12, hand.getBest());
    }

    @Test
    public void getPossibleValuesTwoAceAllOOB() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(9)); // Score 10
        hand.addCard(new Card(9)); // Score 10
        Assert.assertArrayEquals(new int[]{22}, hand.getPossibleValues());
    }

    @Test
    public void getBestTwoAceAllOOB() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(0)); // Score 1 or 11
        hand.addCard(new Card(9)); // Score 10
        hand.addCard(new Card(9)); // Score 10
        Assert.assertEquals(22, hand.getBest());
    }

    @Test
    public void showCard() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Card(0));
        hand.addCard(new Card(1));
        hand.showCard(1);
        List<esi.atl.g40931.blackjack.model.cards.Card> cards = hand.getContent();
        Assert.assertFalse(cards.get(0).isVisible());
        Assert.assertTrue(cards.get(1).isVisible());
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void showCardOOBUp() throws Exception {
        new esi.atl.g40931.blackjack.model.impl.players.Hand().showCard(1);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void showCardOOBDown() throws Exception {
        new esi.atl.g40931.blackjack.model.impl.players.Hand().showCard(-1);
    }

    @Test
    public void addCard() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        Card card = new Card(1);
        hand.addCard(card);
        Field f = esi.atl.g40931.blackjack.model.impl.players.Hand.class.getDeclaredField("content");
        f.setAccessible(true);
        //noinspection unchecked
        List<esi.atl.g40931.blackjack.model.cards.Card> l = (List<esi.atl.g40931.blackjack.model.cards.Card>) f.get(hand);
        Assert.assertEquals(1, l.size());
        Assert.assertEquals(card, l.get(0));
    }

    @Test
    public void isFull() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        Deck deck = new Deck();
        for (int i = 0; i < 7; i++) hand.addCard(deck.pick());
        Assert.assertTrue(hand.isFull());
    }

    @Test(expected = IllegalStateException.class)
    public void addCardFull() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        Deck deck = new Deck();
        for (int i = 0; i < 8; i++) hand.addCard(deck.pick());
    }

}
