package esi.atl.g40931.blackjack.model.players;

import esi.atl.g40931.blackjack.model.impl.cards.Deck;
import org.junit.Assert;
import org.junit.Test;

/**
 * Part of blackjack by Bluexin.
 *
 * @author Bluexin
 */
public class PlayerTest {

    @Test
    public void pick() throws Exception {
        Hand hand = new esi.atl.g40931.blackjack.model.impl.players.Hand();
        hand.addCard(new Deck().pick());
        Player player = new esi.atl.g40931.blackjack.model.impl.players.Player(PlayerType.BANK);
        player.pick(new Deck());
        Assert.assertEquals(hand, player.getHand());
    }

    @Test
    public void bet() throws Exception {
        Player player = new esi.atl.g40931.blackjack.model.impl.players.Player(PlayerType.REGULAR);
        int amount = 50;
        player.bet(amount);
        Assert.assertEquals(player.getInitialMoney() - amount, player.getMoney());
    }

    @Test
    public void earn() throws Exception {
        Player player = new esi.atl.g40931.blackjack.model.impl.players.Player(PlayerType.REGULAR);
        int amount = 50;
        player.earn(amount);
        Assert.assertEquals(player.getInitialMoney() + amount, player.getMoney());
    }

    @Test(expected = IllegalStateException.class)
    public void betBank() throws Exception {
        new esi.atl.g40931.blackjack.model.impl.players.Player(PlayerType.BANK).bet(0);
    }

}
