package esi.atl.g40931.blackjack.model.game;

import esi.atl.g40931.blackjack.model.impl.cards.Card;
import esi.atl.g40931.blackjack.model.impl.cards.Deck;
import esi.atl.g40931.blackjack.model.players.Player;
import esi.atl.g40931.blackjack.model.players.PlayerType;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;

/**
 * Part of blackjack
 *
 * @author G40931
 */
public class BlackjackTest {

    private static final Field CURRENT_PLAYER_FIELD;
    private static final Field ROUND_HANDLER_FIELD;

    static {
        Field f1 = null;
        Field f2 = null;
        try {
            f1 = Class.forName("esi.atl.g40931.blackjack.model.impl.game.RoundHandler").getDeclaredField("currentPlayer");
            f1.setAccessible(true);
            f2 = esi.atl.g40931.blackjack.model.impl.game.Blackjack.class.getDeclaredField("round");
            f2.setAccessible(true);
        } catch (NoSuchFieldException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        CURRENT_PLAYER_FIELD = f1;
        ROUND_HANDLER_FIELD = f2;
    }

    private static void setCurrentPlayer(Blackjack bj, int i) throws Exception {
        CURRENT_PLAYER_FIELD.setInt(ROUND_HANDLER_FIELD.get(bj), i);
    }

    private static int getCurrentPlayer(Blackjack bj) throws Exception {
        return CURRENT_PLAYER_FIELD.getInt(ROUND_HANDLER_FIELD.get(bj));
    }

    @Test
    public void getBank() throws Exception {
        Player p1 = new esi.atl.g40931.blackjack.model.impl.players.Player(PlayerType.BANK);
        Assert.assertEquals(p1, new BlackJackBuilder().addPlayer(p1).addPlayer(PlayerType.REGULAR).build().getBank());
    }

    @Test
    public void getCurrentPlayer() throws Exception {
        Player p1 = new esi.atl.g40931.blackjack.model.impl.players.Player(PlayerType.BANK);
        Player p2 = new esi.atl.g40931.blackjack.model.impl.players.Player(PlayerType.REGULAR);

        Blackjack bj = new BlackJackBuilder().addPlayer(p1).addPlayer(p2).build(); // :wink:

        setCurrentPlayer(bj, 1);
        Assert.assertEquals(p2, bj.getCurrentPlayer());

        setCurrentPlayer(bj, 0);
        Assert.assertEquals(p1, bj.getCurrentPlayer());
    }

    @Test
    public void canHitOk() throws Exception {
        Blackjack bj = new BlackJackBuilder().addPlayer(PlayerType.BANK).addPlayer(PlayerType.REGULAR).build();
        setCurrentPlayer(bj, 0);

        Assert.assertTrue(bj.canHit());
    }

    @Test
    public void canHitNotOk() throws Exception {
        Player p1 = new esi.atl.g40931.blackjack.model.impl.players.Player(PlayerType.BANK);
        p1.getHand().addCard(new Card(9));
        p1.getHand().addCard(new Card(9));
        p1.getHand().addCard(new Card(9));

        Blackjack bj = new BlackJackBuilder().addPlayer(p1).addPlayer(PlayerType.REGULAR).build();
        setCurrentPlayer(bj, 0);

        Assert.assertFalse(bj.canHit());
    }

    @Test(expected = IllegalStateException.class)
    public void canHitISE() throws Exception {
        new BlackJackBuilder().addPlayer(PlayerType.BANK).addPlayer(PlayerType.REGULAR).build().canHit();
    }

    @Test
    public void hit() throws Exception {
        Player p1 = new esi.atl.g40931.blackjack.model.impl.players.Player(PlayerType.BANK);
        Blackjack bj = new BlackJackBuilder().addPlayer(p1).addPlayer(PlayerType.REGULAR).build();

        setCurrentPlayer(bj, 0);
        Field deckField = esi.atl.g40931.blackjack.model.impl.game.Blackjack.class.getDeclaredField("deck");
        deckField.setAccessible(true);
        deckField.set(bj, new Deck());
        synchronized (bj) {
            bj.hit();
            bj.wait();
        }

        Assert.assertEquals(1, p1.getHand().getContent().size());
    }

    @Test(expected = IllegalStateException.class)
    public void hitISERound() throws Exception {
        new BlackJackBuilder().addPlayer(PlayerType.BANK).addPlayer(PlayerType.REGULAR).build().hit();
    }

    @Test(expected = IllegalStateException.class)
    public void hitISEFull() throws Exception {
        Player p1 = new esi.atl.g40931.blackjack.model.impl.players.Player(PlayerType.BANK);
        p1.getHand().addCard(new Card(9));
        p1.getHand().addCard(new Card(9));
        p1.getHand().addCard(new Card(9));

        Blackjack bj = new BlackJackBuilder().addPlayer(p1).addPlayer(PlayerType.REGULAR).build();
        setCurrentPlayer(bj, 0);

        bj.hit();
    }

    @Test
    public void stand() throws Exception {
        Blackjack bj = new BlackJackBuilder().addPlayer(PlayerType.BANK).addPlayer(PlayerType.REGULAR).build();
        setCurrentPlayer(bj, 1);

        synchronized (bj) {
            bj.stand();
            bj.wait();
        }
        Assert.assertEquals(0, getCurrentPlayer(bj));

        synchronized (bj) {
            bj.stand();
            bj.wait();
        }
        Assert.assertEquals(-1, getCurrentPlayer(bj));
    }

    @Test(expected = IllegalStateException.class)
    public void standRoundOver() throws Exception {
        new BlackJackBuilder().addPlayer(PlayerType.BANK).addPlayer(PlayerType.REGULAR).build().stand();
    }

    @Test
    public void isRoundOverOk() throws Exception {
        Assert.assertTrue(new BlackJackBuilder().addPlayer(PlayerType.BANK).addPlayer(PlayerType.REGULAR).build().isRoundOver());
    }

    @Test
    public void isRoundOverNotOk() throws Exception {
        Blackjack bj = new BlackJackBuilder().addPlayer(PlayerType.BANK).addPlayer(PlayerType.REGULAR).build();
        setCurrentPlayer(bj, 1);

        Assert.assertFalse(bj.isRoundOver());
    }

    @Test
    public void nextRound() throws Exception {
        int bet = 30;
        Blackjack bj = new BlackJackBuilder().addPlayer(PlayerType.BANK).addPlayer(PlayerType.REGULAR).setBet(30).build();

        synchronized (bj) {
            bj.nextRound();
            bj.wait();
        }

        Assert.assertEquals(1, getCurrentPlayer(bj));

        Field deckField = esi.atl.g40931.blackjack.model.impl.game.Blackjack.class.getDeclaredField("deck");
        deckField.setAccessible(true);

        Assert.assertTrue(deckField.get(bj) != null);

        bj.getPlayers().forEach(p -> Assert.assertEquals(2, p.getHand().getContent().size()));

        bj.getPlayers().stream().filter(p -> !p.getPlayerType().equals(PlayerType.BANK)).forEach(p -> Assert.assertEquals(p.getInitialMoney() - bet, p.getMoney()));
    }

    @Test(expected = IllegalStateException.class)
    public void nextRoundISE() throws Exception {
        Blackjack bj = new BlackJackBuilder().addPlayer(PlayerType.BANK).addPlayer(PlayerType.REGULAR).build();
        setCurrentPlayer(bj, 1);

        bj.nextRound();
    }

}
