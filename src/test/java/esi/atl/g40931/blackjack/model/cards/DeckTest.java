package esi.atl.g40931.blackjack.model.cards;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Part of blackjack.
 *
 * @author G40931
 */
public class DeckTest {
    @Test
    public void pick() throws Exception {
        Deck deck = new esi.atl.g40931.blackjack.model.impl.cards.Deck();
        for (int i = 0; i < 52; i++)
            Assert.assertEquals(new esi.atl.g40931.blackjack.model.impl.cards.Card(51 - i), deck.pick());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shuffle() throws Exception {
        Deck deck = new esi.atl.g40931.blackjack.model.impl.cards.Deck();
        Deck ref = new esi.atl.g40931.blackjack.model.impl.cards.Deck();

        deck.shuffle(150);

        Field f = esi.atl.g40931.blackjack.model.impl.cards.Deck.class.getDeclaredField("cards");
        f.setAccessible(true);
        List<Card> deckCards = (List<Card>) f.get(deck);
        List<Card> refCards = (List<Card>) f.get(ref);

        Assert.assertTrue(deckCards.containsAll(refCards));
    }

    @Test
    public void equals() throws Exception {
        Deck a = new esi.atl.g40931.blackjack.model.impl.cards.Deck();
        Deck b = new esi.atl.g40931.blackjack.model.impl.cards.Deck();

        Assert.assertEquals(a, b);
    }

    @Test
    public void equalsPicked() throws Exception {
        Deck a = new esi.atl.g40931.blackjack.model.impl.cards.Deck();
        Deck b = new esi.atl.g40931.blackjack.model.impl.cards.Deck();

        for (int i = 0; i < 2; i++) Assert.assertEquals(a.pick(), b.pick());

        Assert.assertEquals(a, b);
    }

    @Test(expected = IllegalStateException.class)
    public void emptyDeck() throws Exception {
        Deck d = new esi.atl.g40931.blackjack.model.impl.cards.Deck();
        //noinspection InfiniteLoopStatement
        while (true) d.pick();
    }

}
