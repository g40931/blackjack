@echo off
setlocal enabledelayedexpansion

echo Blackjack builder utility script by Solé Arnaud
set back=end

:start

set t_start=%back%
set back=start_1
goto checkJava

:start_1

set back=start_2
goto checkJdbc

:start_2

set back=%t_start%

if not exist output mkdir output

if /i "%1" == "clean" goto clean
if /i "%1" == "compile" goto compile
if /i "%1" == "jar" goto jar
if /i "%1" == "test" goto test
if /i "%1" == "run" goto run
if /i "%i" == "test" goto test
goto %back%

:jar



set t_jar=%back%
set back=jar_01
goto clean

:jar_01

set back=jar_1
goto compile

:jar_1

set back=%t_jar%

cd output

echo Main-Class: esi.atl.g40931.blackjack.view.MainMenu > manifest
echo Class-Path: sqlite-jdbc-3.8.11.1.jar >> manifest
%jar% cvfm Blackjack.jar manifest -C ./ . > nul

cd ..

goto %back%

:run

set t_run=%back%
set back=run_1
goto compile

:run_1
set back=%t_run%
java -cp "output;sqlite-jdbc-3.8.11.1.jar" esi.atl.g40931.blackjack.view.MainMenu
goto %back%

:compile

dir /s /B src\main\java\*.java > output\sources-compil.txt
%javac% -g -cp sqlite-jdbc-3.8.11.1.jar -d output @output\sources-compil.txt
if errorlevel 1 (
    echo An error occurred while compiling, aborting...
    del output\sources-compil.txt
    goto real_end
)

del output\sources-compil.txt

echo Done compiling, copying resources...

xcopy src\main\resources output /s /e /y > nul

echo Done copying resources.

goto %back%

:compile-tests

set t_compile-tests=%back%
set back=compile-tests_1
goto compile

:compile-tests_1

set back=%t_compile-tests%

dir /s /B src\test\java\*.java > output\sources-compil-tests.txt
%javac% -g -cp "output;junit-4.12.jar" -d output @output\sources-compil-tests.txt
if errorlevel 1 (
    echo An error occurred while compiling tests, aborting...
    del output\sources-compil-tests.txt
    goto real_end
)

del output\sources-compil-tests.txt

echo Done compiling tests

goto %back%

:test

set t_test=%back%
set back=test_1
goto compile-tests

:test_1

set back=%t_test%

set current_dir=%~dp0
for /f %%f in ('dir /s /B output\*Test.class') do (
    set found_file=%%f
    set class_name=!found_file:%current_dir%=!
    set class_name=!class_name:output\=!
    set class_name=!class_name:\=.!
    set class_name=!class_name:.class=!
    echo Texting %class_name%...
    java -cp "junit-4.12.jar;hamcrest-core-1.3.jar;output" org.junit.runner.JUnitCore %class_name%
)
echo Testing finished !

goto %back%

:clean

rmdir /s /q output

goto %back%

:checkJava

javac -version 2> nul
if not errorlevel 1 (
    set javac=javac
    set jar=jar
    goto %back%
)

:: javac not on path, checking JAVA_HOME
set JAVA_EXE=%JAVA_HOME%/bin/java.exe
if exist "%JAVA_EXE%" (
    set javac="%JAVA_HOME%\bin\javac"
    set jar="%JAVA_HOME%\bin\jar"
    goto %back%
)

echo ERROR: javac cannot be found and JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo Please set the JAVA_HOME variable in your environment to match the location of your Java installation.
goto real_end

:checkJdbc

if exist sqlite-jdbc-3.8.11.1.jar goto %back%

echo ERROR: SQLite library not found.
echo Please add sqlite-jdbc-3.8.11.1.jar to current directory.
goto real_end

:end
echo Done!

:real_end
