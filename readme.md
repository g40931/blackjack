Blackjack
==========

By Arnaud Solé (G40931) for HE2B/ESI
==

Little java project letting you play the Blackjack game, written in Java 8 with JavaFX.

Licence info for images can be found at [resources/Licence.md](/src/main/resources/Licence.md)


How to run this project
====

Using maven :
==

Running the app :
```
mvn exec:java
```

Packaging the app :
```
mvn install
```

Running unit tests :
```
mvn test
```

Generating javadoc :
```
mvn javadoc:javadoc
```
(index will be at [current directory]/target/site/apidocs/index.html)

Remove all maven-generated files :
```
mvn clean
```


Without maven (windows) :
==

All of these require you to have Xerial's SQLite driver library (sqlite-jdbc-3.8.11.1.jar) in the current directory

Note:
Due to no version controlling, all commands will also trigger prerequisites: for example, packaging (jar) will always trigger fresh compilation.

Running the app :
```
blackjack run
```

Compiling the app :
```
blackjack compile
```

Packaging the app :
```
blackjack jar
```

Running unit tests :
```
blackjack test
```
Requires junit 4 (junit-4.12.jar) and Hamcrest Core (hamcrest-core-1.3.jar) in the current directory.

Generating javadoc :
```
TODO
```

Remove all generated files :
```
blackjack clean
```
